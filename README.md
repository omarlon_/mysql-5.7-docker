# Mysql 5.7 Docker

Este é um repositório que contém uma imagem do Mysql 5.7

## Installation

- Clonar num diretório a pasta do projeto

```bash
cd pasta_onde_voce_extraiu
docker-compose up -d
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
